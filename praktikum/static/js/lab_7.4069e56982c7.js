     $(document).ready(function() {
        $.ajax({
            url : '{% url "lab-7:get-friend-list" %}',
            success : function (response){
                sessionStorage.setItem("data", JSON.stringify(response));
            }
        });
    });






    var addFriend = function(nama, npm) {
        $.ajax({
            method: "POST",
            url: '{% url "lab-7:add-friend" %}',
            data: { 'name': nama, 'npm': npm},
            success : function (friend) {
                friend = JSON.parse(friend)
                console.log(friend.friend_name)
                html = '<a class="list-group-item clearfix">' + friend.friend_name  + ' (' + friend.npm + ')'+ '</a>';
                $("#friend-list").append(html)
            },
            error : function (error) {
                alert("Mahasiswa tersebut sudah ditambahkan sebagai teman")
            }
        });
    };

    $("#add-friend").on("submit", function () {
       name = $("#field_name").val()
       npm = $("#field_npm").val()
        exists = false
                    response = JSON.parse(sessionStorage.getItem("data"));
                    person = {};
                    if(name.length==0 || npm.length==0){
                        alert("Mohon lengkapi data");
                        event.preventDefault();
                    }else{
                        for(var i=0; i<100; i++){
                            if(npm==response.json[i].npm&&name.toLowerCase()==response.json[i].nama.toLowerCase()){
                                person['nama'] = response.json[i].nama;
                                person['npm'] = response.json[i].npm;
                                exists = true;
                                break;
                            }
                        }
                        if(exists){
                            $("#field_name").val("")
                            $("#field_npm").val("")
                            addFriend(person.nama, person.npm)
                            event.preventDefault();
                        }else{
                            alert("Mahasiswa tidak ditemukan");
                            $("#field_name").val("")
                            $("#field_npm").val("")
                            event.preventDefault();
                        }
                    }
                });


    $("#field_npm").change(function () {
        console.log( $(this).val() );
        npm = $(this).val();
        $.ajax({
            method: "POST",
            url: '{% url "lab-7:validate-npm" %}',
            data: {
                'npm': npm
            },
            dataType: 'json',
            success: function (data) {
                console.log(data)
                if (data.is_taken) {
                    alert("Mahasiswa dengan npm seperti ini sudah ada");
                    $("#field_npm").val("")
                }
            }
        });
    });