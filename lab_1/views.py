from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Andrew Savero Ongko' # TODO Implement this
birth_date = date(1999,8,30)
year_now = int(datetime.now().strftime("%Y"))

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year)}
    return render(request, 'index_lab1.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    return year_now - birth_year if birth_year <= year_now else 0

