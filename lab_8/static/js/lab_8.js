window.fbAsyncInit = function() {
    FB.init({
      appId      : '2134793119868046',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });
    
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
          $('#login-button').remove();
          render(true);
      }
  });
  
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  
const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login
      // Memanggil method getUserData (lihat ke bawah) yang diimplementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.

      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        html = '<div class="profile">' +
            '<div class="crop-home-pic"><img class="cover" src="' + user.cover.source + '" alt="cover" /></div>' +
            '<div id="line-cover"></div>' + '<div id="line-picture-name"><div class="prof-pic-container">' +
            '<img class="prof-pic" src="' + user.picture.data.url + '" alt="profpic" /></div>' +
            '<h1 id="prof-name">' + user.name + '</h1><h2>' + user.email;
        if (user.gender) {
            html += ' - ' + user.gender;
        }
        html += '</h2>'
        
        if (user.about) {
            html += '<h3>' + user.about + '</h3>';
        } else {
            html += '<h3 style="color:#B3B9BF">No description available</h3>';
        }
        html += '</div></div>';
        html += '<button class="logout" onclick="facebookLogout()">Logout</button>';
        $('#lab-8-profile').html(html);
        $('#login-button').remove();

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        var feedExist = false;
        getUserFeed(feed => {
          if (feed.data) {
              $('#lab-8-post-input').html(
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>'
          );
          }
          console.log(feed.data);
          feed.data.map(value => {
            var feedhtml = '';
            var feedcontent = '';
            if (value.message || value.story) {
                feedhtml = '<div class="feed"><div class="feed-author">'
                + '</h2> <img class="feed-author-profpic" src="http://graph.facebook.com/' + value.from.id + '/picture?type=square">';
                if (value.story) {
                     feedhtml += '<h2>' + value.story + '</h2></div>';
                } else {
                     feedhtml += '<h2 class="feed-author-name">' + value.from.name + '</h2></div>';
                     feedcontent = value.message;
                }
                feedhtml += '<div class="content">' + feedcontent + '</div></div>';
            }
            $('#lab-8-feed').append(feedhtml);
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $("#lab-8-profile").empty();
      $('#lab-8-profile').html('<button class="login" onclick="facebookLogin()">Login with Facebook</button>');
      $("#lab-8-post-input").empty();
      $("#lab-8-feed").empty();
    }
  };

  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    // Facebook permission: https://developers.facebook.com/docs/facebook-login/permissions/
    
     FB.login(function(response){
       render(response.status === 'connected');
     }, {scope:'public_profile,email,user_about_me,user_posts,publish_actions'})
     
     
  };

  const facebookLogout = () => {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.logout();
            render(false);
            console.log('Logged out');
        }
    });
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  
  // helper link: https://developers.facebook.com/docs/php/howto/example_retrieve_user_profile
  const getUserData = (funct) => {
      FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            FB.api('/me?fields=id,name,cover,email,gender,picture,about', 'GET', function (response){
            console.log(response);
            funct(response);
        });
        }
      });
  };

  const getUserFeed = (funct) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me/feed?fields=story,message,from,created_time', 'GET', function(response){
            funct(response);

          });
        }
    });
  };

  const postFeed = () => {
     message = $('#postInput').val().trim();
     name = $('#prof-name').text();
     if (message.length != 0) {
     FB.api('/me/feed', 'POST', {message:$('#postInput').val()});
     feedhtml = '<div class="feed"><div class="feed-author">'
                + '</h2><img class="feed-author-profpic" src="' + $('.prof-pic').attr('src') + '"><h2 class="feed-author-name">' + name + '</h2></div>' + '<div class="feed-content">' + message + '</div></div>';
     $(feedhtml).prependTo('#lab-8-feed');
     }
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };
